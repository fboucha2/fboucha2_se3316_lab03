<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=us-ansi">
        <meta name="viewport" content="width=device-width, initial-scale=1">
  
        <title>Felix Bouchard Lab3 SE3316A</title>
        
        <link rel="stylesheet" href="Resources/bootstrap/css/bootstrap.css"> 
        <link rel="stylesheet" href="Resources/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="Resources/bootstrap/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="Resources/bootstrap/css/bootstrap-theme.css">
        
        <script src="Resources\boostrap\js\bootstrap.min.js"></script>
        
        
        <script language="JavaScript" type="text/javascript" src="jquery.js"></script>
        
        <link href="https://fonts.googleapis.com/css?family=Cuprum|Cookie" rel="stylesheet" type="text/css">
        
        <script src="../../assets/js/ie-emulation-modes-warning.js"></script>
        <link href="CarouselStyle.css" rel="stylesheet">
        
        <script language="JavaScript" type="text/javascript">
            $(document).ready(function(){
            $('.myCarousel').carousel({
                interval: 500
            })
            });    
        </script>
       
  </style>
    </head>
    <body>
        <div class="navbar-wrapper">
            <div class="container">
                <div class="navbar navbar-static-top" role="navigation">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">LAB 3</a>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="index.php">Home</a></li>
                                <li><a href="about.php">About</a></li>
                                <li><a href="work.php">Work</a></li>  
                                <li><a href="artists.php">Artists</a></li>                
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                <li data-target="#myCarousel" data-slide-to="2" class=""></li>
                <li data-target="#myCarousel" data-slide-to="3" class=""></li>
                <li data-target="#myCarousel" data-slide-to="4" class=""></li>
            </ol>
              
            <div class="carousel-inner">
                <?PHP 
                $PaintArr = file("Resources/data-files/paintings.txt");
                for($i =0 ; $i<5; $i++){
                if($i==0)
                echo '<div class="item active">';
                else echo '<div class="item">';
                
                $Paint = explode('~', $PaintArr[$i]);
                echo '<img src="Resources/art-images/paintings/medium/'.$Paint[3].'.jpg" alt="'.$Paint[4].'" title="'.$Paint[4].'" rel="#PaintingThumb" /> 
                <div class="container">
                    <div class="carousel-caption">
                        <h1>'.$Paint[4].'</h1>
                        <p>'.$Paint[6].'</p>
                        <p><a class="btn btn-lg btn-primary" href="'.$Paint[12].'" role="button">Learn more</a></p>
                    </div>
                     </div>
                 </div>';
                }
                
                ?>
            </div>
            
            
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
        </div>
        
     
        <div class="container marketing">
        <?PHP 
        ///include "Resources/data-files/artists.txt";
        
        $PaintArr = file("Resources/data-files/paintings.txt");
        for ($i=5; $i<11; $i++)
        {
            if($i==5 || $i==8)
            {
                echo '<div class="row">';
            }
            $delimiter = '~';
            $newPA =explode($delimiter, $PaintArr[$i]);
            echo '<div class= "col-lg-4"><img class= "img-circle" src="Resources/art-images/paintings/medium/' .$newPA[3] .'.jpg" style="width:100px; height:100px;">';
            echo '<h1>'.$newPA[4] .'</h1>';
            $desc= explode('.',$newPA[5]);
            echo '<p class="text-justify">'.$desc[0] .'</p>';
            echo '<p><a class="btn btn-default" href="' . $newPA[12].'" role="button">View details &raquo;</a></p></div>';
            if($i==7 || $i==10)
            {
                echo '</div>';
            }
            
        }
        ?>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="Resources/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>